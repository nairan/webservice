package com.webservice.webservice.Repository;

import java.util.List;
import com.webservice.webservice.Model.Login;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginRepository extends JpaRepository<Login, Integer> {
    List<Login> findByEmail(String email);
}
