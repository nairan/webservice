package com.webservice.webservice.Repository;

import java.util.List;

import com.webservice.webservice.Model.Upload;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UploadRepository extends JpaRepository<Upload, Integer> {
    List<Upload> findById(int id);
}
