package com.webservice.webservice;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping(
        value = "/principal", 
        //params = { "id", "name", "email" }, 
        method = RequestMethod.GET
    )
    public String principal(
        //@RequestParam("id") String id, 
        //@RequestParam("name") String name,
        //@RequestParam("email") String email
    ){

        //Autentication auth = new Autentication();
        //auth.validate(email);
        //System.out.println(email);
        return "principal";
    }

    @ResponseBody
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String upload() {


        return null;
    }
    

    @ResponseBody
    @RequestMapping(value = "/autentication", method = RequestMethod.POST)
    public String validateAutentication() {
        return "teste";
    }
}