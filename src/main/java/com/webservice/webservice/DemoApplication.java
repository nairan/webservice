package com.webservice.webservice;

import com.webservice.webservice.Repository.LoginRepository;
import com.webservice.webservice.Repository.UploadRepository;
import com.webservice.webservice.Controller.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties({
	FileStorageProperties.class
})
public class DemoApplication {

	private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);
	public static LoginRepository lrepository;
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class);
	}

	@Bean
	public CommandLineRunner demo(LoginRepository lRepository, UploadRepository uRepository) {
		return (args) -> {
			/** for (Upload upload : uRepository.findAll()) {
				System.out.println(upload);
			}*/

			DemoApplication.lrepository = lRepository;
		};
	}
}