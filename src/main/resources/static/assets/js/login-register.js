
function renderButton() { //https://developers.google.com/identity/sign-in/web/sign-in
    gapi.signin2.render('my-signin2', {
        'scope': 'profile email',
        'width': '280',
        'height': 48,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': function (googleUser) {
            let perfil = googleUser.getBasicProfile();
            //let userPicture = perfil.getImageUrl();
            $.post(
                "/principal", { 
                    id: perfil.getId(), 
                    name: perfil.getName(),
                    email: perfil.getEmail()
                }).success(function(data) {
                    window.location.assign("/principal");
                }).error(function() {
                    console.log("error");
                });
        },
        'onfailure':  function onFailure(error) {
            console.log(error);
        }
    });
  }
/*
 *
 * login-register modal
 * Autor: Creative Tim
 * Web-autor: creative.tim
 * Web script: http://creative-tim.com
 * 
 */
function showRegisterForm(){
    $('.loginBox').fadeOut('fast',function(){
        $('.registerBox').fadeIn('fast');
        $('.login-footer').fadeOut('fast',function(){
            $('.register-footer').fadeIn('fast');
        });
        $('.modal-title').html('Registrar com');
    }); 
    $('.error').removeClass('alert alert-danger').html('');
       
}
function showLoginForm(){
    $('#loginModal .registerBox').fadeOut('fast',function(){
        $('.loginBox').fadeIn('fast');
        $('.register-footer').fadeOut('fast',function(){
            $('.login-footer').fadeIn('fast');    
        });
        
        $('.modal-title').html('Acessar com');
    });       
     $('.error').removeClass('alert alert-danger').html(''); 
}

function openLoginModal(){
    showLoginForm();
    setTimeout(function(){
        $('#loginModal').modal('show');    
    }, 230);
    
}
function openRegisterModal(){
    showRegisterForm();
    setTimeout(function(){
        $('#loginModal').modal('show');    
    }, 230);
    
}

function loginAjax(){
    /*   Remove this comments when moving to server
    $.post( "/login", function( data ) {
            if(data == 1){
                window.location.replace("/home");            
            } else {
                 shakeModal(); 
            }
        });
    */

/*   Simulate error message from the server   */
     shakeModal();
}

function shakeModal(){
    $('#loginModal .modal-dialog').addClass('shake');
             $('.error').addClass('alert alert-danger').html("combinação de email/senha inválida!");
             $('input[type="password"]').val('');
             setTimeout( function(){ 
                $('#loginModal .modal-dialog').removeClass('shake'); 
    }, 1000 ); 
}

   